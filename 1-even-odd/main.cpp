#include <iostream>
using namespace std;

int main() {

    /*
        Ask user for 5 integers.
        Classify each one as even or odd.
        Even numbers leave a remainder of 0 when divided by 2
    */

    // modulo operator gives us the remainder
    // 5 / 2 = 2 rem 1
    // 10 / 2 = 5 rem 0

    int x;
    for (int i = 0; i < 5; i++) {
        cin >> x;

        if (x % 2 == 0) {
            cout << "The number is even" << endl;
        } else {
            cout << "The number is odd" << endl;
        }
    }

    return 0;
}