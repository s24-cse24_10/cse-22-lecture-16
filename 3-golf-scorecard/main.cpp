#include <iostream>
using namespace std;

void printBorder() {
    cout << ".---------------------------------------------------------------------------------------------------------------------------------------------------." << endl;
}

void printCourseName() {
    string name;
    getline(cin, name);

    cout << "| " << name;
    int spaces = 149 - name.length() - 4;
    for (int i = 0; i < spaces; i++) {
        cout << " ";
    }
    cout << " |" << endl;
}

void printDivider() {
    cout << "|---------------------------------------------------------------------------------------------------------------------------------------------------|" << endl;
}

void printHoles() {
    cout << "| Hole             |";
    for (int i = 1; i <= 9; i++) {
        cout << "  " << i << "  |";
    }
    cout << " OUT  |";

    for (int i = 10; i <= 18; i++) {
        cout << " " << i << "  |";
    }
    cout << "  IN  | TOT  |" << endl;
}

void printBlueYardages() {
    // frontNine = frontNine + yards

    cout << "| Blue             |";

    int frontNine = 0;
    for (int i = 0; i < 9; i++) {
        int yards;
        cin >> yards;
        frontNine += yards;     
        cout << " " << yards << " |";
    }
    cout << " " << frontNine << " |";

    int backNine = 0;
    for (int i = 0; i < 9; i++) {
        int yards;
        cin >> yards;
        backNine += yards;     
        cout << " " << yards << " |";
    }
    cout << " " << backNine << " |";

    int total = frontNine + backNine;
    cout << " " << total << " |" << endl;
}

int main() {

    /*
        Getting started on Exercise 3 of Lab 8
    */

    printBorder();
    printCourseName();
    printDivider();
    printHoles();
    printDivider();
    printBlueYardages();

    return 0;
}