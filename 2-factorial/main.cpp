#include <iostream>
using namespace std;

int main() {

    /*
        Ask user to input a number.
        Find the factorial of that number.
        4! = 4 * 3 * 2 * 1 = 24
    */
    
    int x;
    cin >> x;

    int factorial = 1;

    cout << x << "! = ";
    for (int i = x; i >= 1; i--) {
        cout << i;
        // if i is bigger than 1, we are not at the last number
        if (i > 1) {
            cout << " * ";
        }
        factorial = factorial * i;
    }
    cout << " = " << factorial << endl;

    return 0;
}